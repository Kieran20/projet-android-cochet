package fr.cochet_doucet_voisin.e_training.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.ArrayRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.annotation.Retention;

import fr.cochet_doucet_voisin.e_training.R;



public class IconAndTextAdapter extends ArrayAdapter<String> {
    private final int icons;
    private final Context context;
    private final TypedArray tab_images_pour_la_liste;
    private final int mylayout;
    private int tv;
    private int iv;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(this.mylayout, parent, false);

        TextView textView = (TextView) rowView.findViewById(this.tv);
        ImageView imageView = (ImageView) rowView.findViewById(this.iv);

        textView.setText(getItem(position));

        if (convertView == null) {
            imageView.setImageResource(tab_images_pour_la_liste.getResourceId(position, -1));
            Log.d(MainActivity.APPLICATION_TAG, Integer.toString(tab_images_pour_la_liste.getResourceId(position, -1)));
        }
        else
            rowView = (View) convertView;

        return rowView;
    }

    public IconAndTextAdapter(Context context, @LayoutRes int layout, String[] values, @ArrayRes int icons, int txtView, int imgView) {
        super(context, layout, values);
        this.mylayout = layout;
        this.icons = icons;
        this.context = context;
        this.tab_images_pour_la_liste = context.getResources().obtainTypedArray(icons);

        for (int i = 0; i < tab_images_pour_la_liste.length(); i++)
            Log.d(MainActivity.APPLICATION_TAG, Integer.toString(tab_images_pour_la_liste.getResourceId(i, -1)));
        this.tv = txtView;
        this.iv = imgView;
    }
}
