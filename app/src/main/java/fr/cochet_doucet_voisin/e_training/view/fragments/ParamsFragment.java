package fr.cochet_doucet_voisin.e_training.view.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.cochet_doucet_voisin.e_training.R;
import fr.cochet_doucet_voisin.e_training.view.MainActivity;


public class ParamsFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(MainActivity.APPLICATION_TAG, "openning ParamsFragment");
        return inflater.inflate(R.layout.fragment_param, container, false);
    }
}
