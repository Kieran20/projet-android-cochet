package fr.cochet_doucet_voisin.e_training.view;

import android.app.Fragment;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.annotation.IntDef;
import android.support.annotation.IntegerRes;
import android.support.annotation.StringRes;
import android.support.annotation.StyleableRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

import fr.cochet_doucet_voisin.e_training.R;
import fr.cochet_doucet_voisin.e_training.view.fragments.MainFragment;
import fr.cochet_doucet_voisin.e_training.view.fragments.ParamsFragment;
import fr.cochet_doucet_voisin.e_training.view.fragments.ProgramFragment;
import fr.cochet_doucet_voisin.e_training.view.fragments.StatFragment;

public class MainActivity extends AppCompatActivity {
    DrawerMenu menu;
    public static final String APPLICATION_TAG = "E-Eraining V2";
    private ArrayList<Fragment> mesFragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.menu = new DrawerMenu(this);
        this.mesFragments = new ArrayList<>();
        this.addFragment(new MainFragment());
    }

    public void notifyDrawerMenuAction(@DrawerMenu.DrawerMenuAction final int action, Bundle data) {
        switch (action) {
            case DrawerMenu.ACTION_OPENED:
                Log.v(MainActivity.APPLICATION_TAG, "Drawer menu opened");
                break;
            case DrawerMenu.ACTION_CLOSED:
                Log.v(MainActivity.APPLICATION_TAG, "Drawer menu closed");
                break;
            case DrawerMenu.ACTION_ITEM_CLICK:
                // Bundle parameters: int PARAM_ITEM_CLICK_POSITION, long PARAM_ITEM_CLICK_ID
                Log.v(MainActivity.APPLICATION_TAG, "Drawer menu item click: position="
                        + Integer.toString(data.getInt(DrawerMenu.PARAM_ITEM_CLICK_POSITION))
                        + " id=" + Long.toString(data.getLong(DrawerMenu.PARAM_ITEM_CLICK_ID)));
                changeFragmentsFor(data.getInt(DrawerMenu.PARAM_ITEM_CLICK_POSITION));
                break;
            case DrawerMenu.ACTION_SLIDE:
                // Bundle parameters: float PARAM_SLIDE_OFFSET

                break;
            case DrawerMenu.ACTION_DRAWER_STATE_CHANGED:
                // Bundle parameters: int PARAM_STATE_CHANGED_NEW_STATE

                break;
            default:

        }
    }

    private void changeFragmentsFor(int anInt) {
        TypedArray ar = getResources().obtainTypedArray(R.array.menu_items_str);
        for (int i = 0; i < ar.length(); i++)
            Log.d(MainActivity.APPLICATION_TAG, Integer.toString(ar.getResourceId(i, -1)));
        switch (ar.getResourceId(anInt, -1)) {
            case R.string.launch_seance:
                removeAllFragments();
                addFragment(new MainFragment());
                break;
            case R.string.create_program:
                removeAllFragments();
                addFragment(new ProgramFragment());
                break;
            case R.string.stats:
                removeAllFragments();
                addFragment(new StatFragment());
                break;
            case R.string.parameters:
                removeAllFragments();
                addFragment(new ParamsFragment());
                break;
        }
    }

    public void addFragment(Fragment fragment) {
        android.app.FragmentManager fm = getFragmentManager();
        fm.beginTransaction()
                .add(R.id.body, fragment)
                .setTransition(android.app.FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
        this.mesFragments.add(fragment);
    }

    public void removeFragment(Fragment fragment) {
        if (this.mesFragments.contains(fragment)) {
            android.app.FragmentManager fm = getFragmentManager();
            fm.beginTransaction()
                    .remove(fragment)
                    .setTransition(android.app.FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .commit();
            this.mesFragments.remove(fragment);
        }
    }

    public void removeAllFragments() {
        while (this.mesFragments.size() > 0)
            this.removeFragment(this.mesFragments.get(0));
    }
}
