package fr.cochet_doucet_voisin.e_training.view;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringDef;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import fr.cochet_doucet_voisin.e_training.R;



class DrawerMenu {
    private MainActivity context;
    @SuppressWarnings("WeakerAccess")
    protected ListView menu;
    @SuppressWarnings("WeakerAccess")
    protected DrawerLayout dlayout;


    DrawerMenu(MainActivity context) {
        this.context = context;
        this.menu = (ListView) this.context.findViewById(R.id.menu);
        this.dlayout = (DrawerLayout) this.context.findViewById(R.id.drawer_layout);
        this.menu.setAdapter(
                new IconAndTextAdapter(
                        this.context.getApplicationContext(),
                        R.layout.menu_list_layout,
                        this.context.getResources().getStringArray(R.array.menu_items_str),
                        R.array.menu_items_drawable,
                        R.id.menu_list_TextView,
                        R.id.menu_list_ImageView)
        );
//        String[] ar = this.context.getResources().getStringArray(R.array.menu_items_str);
//        this.menu.setAdapter(new ArrayAdapter<String>(this.context,R.layout.menu_list_layout,ar));
        DrawerMenuListener dmlist = new DrawerMenuListener(this);

        this.menu.setOnItemClickListener(dmlist);

        this.dlayout.addDrawerListener(dmlist);


    }


    @Retention(RetentionPolicy.SOURCE)
    @IntDef(value = {DrawerLayout.STATE_IDLE, DrawerLayout.STATE_DRAGGING, DrawerLayout.STATE_SETTLING})
    @interface DrawerStateParam {
    }

    static final int ACTION_OPENED = 1, ACTION_CLOSED = 2, ACTION_SLIDE = 3,
            ACTION_ITEM_CLICK = 4, ACTION_DRAWER_STATE_CHANGED = 5;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef(value = {DrawerMenu.ACTION_OPENED, DrawerMenu.ACTION_CLOSED, DrawerMenu.ACTION_SLIDE,
            DrawerMenu.ACTION_ITEM_CLICK, DrawerMenu.ACTION_DRAWER_STATE_CHANGED})
    @interface DrawerMenuAction {
    }


    public static final String PARAM_ITEM_CLICK_POSITION = "ITEM_CLICK_POSITION";
    public static final String PARAM_ITEM_CLICK_ID = "ITEM_CLICK_ID";
    public static final String PARAM_SLIDE_OFFSET = "PARAM_SLIDE_OFFSET";
    public static final String PARAM_STATE_CHANGED_NEW_STATE = "PARAM_STATE_CHANGED_NEW_STATE";


    private class DrawerMenuListener implements AdapterView.OnItemClickListener, DrawerLayout.DrawerListener {
        DrawerMenu context;

        DrawerMenuListener(DrawerMenu drawerMenu) {
            this.context = drawerMenu;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Bundle bundle = new Bundle();
            bundle.putInt(DrawerMenu.PARAM_ITEM_CLICK_POSITION, position);
            bundle.putLong(DrawerMenu.PARAM_ITEM_CLICK_ID, id);
            this.context.notifyDrawerAction(DrawerMenu.ACTION_ITEM_CLICK, bundle);
//            Log.v(MainActivity.APPLICATION_TAG,"action performed over DrawerMenu");
        }

        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {
            Bundle bundle = new Bundle();
            bundle.putFloat(DrawerMenu.PARAM_SLIDE_OFFSET, slideOffset);
            this.context.notifyDrawerAction(DrawerMenu.ACTION_SLIDE, bundle);

        }

        @Override
        public void onDrawerOpened(View drawerView) {
            Bundle bundle = new Bundle();
            this.context.notifyDrawerAction(DrawerMenu.ACTION_OPENED, bundle);
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            Bundle bundle = new Bundle();
            this.context.notifyDrawerAction(DrawerMenu.ACTION_CLOSED, bundle);
        }

        @Override
        public void onDrawerStateChanged(@DrawerStateParam int newState) {
            Bundle bundle = new Bundle();
            bundle.putInt(DrawerMenu.PARAM_STATE_CHANGED_NEW_STATE, newState);
            this.context.notifyDrawerAction(DrawerMenu.ACTION_DRAWER_STATE_CHANGED, bundle);
        }
    }

    private void notifyDrawerAction(@DrawerMenuAction final int action, Bundle data) {
        this.context.notifyDrawerMenuAction(action, data);
    }

}
