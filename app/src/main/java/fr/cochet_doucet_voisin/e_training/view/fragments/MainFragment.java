package fr.cochet_doucet_voisin.e_training.view.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import fr.cochet_doucet_voisin.e_training.R;
import fr.cochet_doucet_voisin.e_training.view.MainActivity;

public class MainFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(MainActivity.APPLICATION_TAG, "openning MainFragment");
        return inflater.inflate(R.layout.fragment_main, container, false);
    }
}
