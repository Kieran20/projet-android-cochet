package fr.cochet_doucet_voisin.e_training.view.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import fr.cochet_doucet_voisin.e_training.R;
import fr.cochet_doucet_voisin.e_training.view.MainActivity;




public class ProgramFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.v(MainActivity.APPLICATION_TAG, "openning ProgramFragment");

        return inflater.inflate(R.layout.fragment_program, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        final String[] items = getResources().getStringArray(R.array.menu_items_programme);
        final ArrayAdapter<String> stat = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, items);
        ListView lv =(ListView) getActivity().findViewById(R.id.listView1);
        lv.setAdapter(stat);
    }
}
